const http = require('http');

const server = http.createServer((request, response) => {
    response.statusCode = 200;
    response.setHeader('Content-Type', 'text/plain');
    response.end('Welcome to the 3rd dimension\n');
});

server.listen(3000, () => {
    console.log('Server running on port 3000');
})