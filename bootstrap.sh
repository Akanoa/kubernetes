apt update
apt install docker.io
snap install kubectl --classic
k3d cluster create mycluster --agents 3 --k3s-server-arg --disable=traefik -p "30000-32767:30000-32767@server[0]"
kubectl config use-context k3d-mycluster

kubectl apply -f https://www.getambassador.io/yaml/ambassador/ambassador-crds.yaml
kubectl apply -f https://www.getambassador.io/yaml/ambassador/ambassador-rbac.yaml
kubectl apply -f https://www.getambassador.io/yaml/ambassador/ambassador-service.yaml

kubectl create -f quote.yml
kubectl create -f quote-mapping.yml